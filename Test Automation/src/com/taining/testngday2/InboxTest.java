package com.taining.testngday2;

import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class InboxTest {
	Reporter report = new Reporter();
	
	@BeforeMethod
	public void launchBrowser() {
	System.out.println("This Launch Browser");
	}
	

@Test

public void TC_1() {
	report.log("Test Case with Valid User Name and Password");
	System.out.println("This the first TestNG class");
}

@Test()
public void TC_2() {
	report.log("Test Case with invalid User Name and Password");
	System.out.println("This the Second TestNG class");
}

@Test()
public void TC_3() {
	report.log("Test Case with invalid password and Valid user Name");
	System.out.println("This the third TestNG class");
}
	
@AfterMethod
public void closBrowser() {
System.out.println("This is close Browser");
}

@BeforeClass


public void start() {
System.out.println("Ready for test");
}

}
