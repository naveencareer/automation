package com.training.testngday1;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestNGDemo2 {

	@BeforeMethod
		public void launchBrowser() {
		System.out.println("This Launch Browser");
		}
		
	
	@Test
	public void TC_4() {
		System.out.println("This the fourth TestNG class");
	}
	
	@Test()
	public void TC_5() {
		System.out.println("This the fifth TestNG class");
	}
	
	@Test()
	public void TC_6() {
		System.out.println("This the sixth TestNG class");
	}
		
	@AfterMethod
	public void closBrowser() {
	System.out.println("This is close Browser");
	}
	
	@BeforeClass
	
	
	public void start() {
	System.out.println("Ready for test");
	}
	
	
	
}
