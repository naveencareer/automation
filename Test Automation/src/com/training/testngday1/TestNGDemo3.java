package com.training.testngday1;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestNGDemo3 {
	@BeforeMethod
	public void launchBrowser() {
	System.out.println("This Launch Browser");
	}
	

	@Test
	public void TC_7() {
	System.out.println("This the seventh TestNG class");
	}

	@Test()
	public void TC_8() {
	System.out.println("This the eigth TestNG class");
	}

	@Test()
	public void TC_9() {
	System.out.println("This the ninth TestNG class");
	}
	
	@AfterMethod
	public void closBrowser() {
	System.out.println("This is close Browser");
	}

	@BeforeClass
	public void start() {
	System.out.println("Ready for test");
	}


	}
