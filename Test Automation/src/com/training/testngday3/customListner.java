package com.training.testngday3;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class customListner extends TestListenerAdapter{
	public static WebDriver driver;
	@Override
	public void onTestFailure(ITestResult tr) {
		EventFiringWebDriver eDriver =new EventFiringWebDriver(ParallelTestExecution.driver);
		File src = eDriver.getScreenshotAs(OutputType.FILE);
		File target = new File("D:\\Selenium-Training\\screenshot\\" + tr.getName() + ".jpg");
		try {
			FileUtils.copyFile(src, target);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
