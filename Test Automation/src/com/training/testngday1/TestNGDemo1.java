package com.training.testngday1;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestNGDemo1 {

	@BeforeMethod
		public void launchBrowser() {
		System.out.println("This Launch Browser");
		}
		
	
	@Test
	public void TC_1() {
		System.out.println("This the first TestNG class");
	}
	
	@Test()
	public void TC_2() {
		System.out.println("This the Second TestNG class");
	}
	
	@Test()
	public void TC_3() {
		System.out.println("This the third TestNG class");
	}
		
	@AfterMethod
	public void closBrowser() {
	System.out.println("This is close Browser");
	}
	
	@BeforeClass
	
	
	public void start() {
	System.out.println("Ready for test");
	}
	
	
	
}
