package com.training.testngday3;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
public class ParallelTestExecution {

	public static WebDriver driver;
	@Parameters("myBrowser")
	@BeforeTest
	public void launchBrowser(String myBrowser) {
	if(myBrowser.equalsIgnoreCase("firefox")) {
		driver =new FirefoxDriver();
	}
	else if(myBrowser.equalsIgnoreCase("chrome")) {
		System.setProperty("webdriver.chrome.driver", "D:\\Softwares\\chromedriver_win32\\chromedriver.exe");
		driver=new ChromeDriver();
	}
	}
	
	@Test(groups = {"retest"})
	public void TC_1() throws IOException {
		System.out.println("Hit the URL");
		driver.get("https://www.flipkart.com/");
		/*EventFiringWebDriver eDriver =new EventFiringWebDriver(driver);
		File src = eDriver.getScreenshotAs(OutputType.FILE);
		File target = new File("D:\\Selenium-Training\\screenshot\\flipkart.jpg");
		FileUtils.copyFile(src, target);*/
		Assert.assertEquals("test","test123");
		}
	
	@Test(groups = {"smoke"})
	public void TC_2() {
		System.out.println("Second Test Case");
		driver.get("https://www.amazon.com/");
		Assert.assertEquals("test","test123");
		
		}
	
	@Test(groups = {"sanity"})
	public void TC_3() {
		System.out.println("Third Test Case");
		driver.get("https://www.google.co.in/");
		Assert.assertEquals("test","test123");
		}
}
