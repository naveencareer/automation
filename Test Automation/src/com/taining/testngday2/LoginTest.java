package com.taining.testngday2;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginTest {
	@BeforeMethod
	public void launchBrowser() {
	System.out.println("This Launch Browser");
	}
	

@Test
public void TC_4() {
	System.out.println("This the first TestNG class");
	Assert.assertEquals("test1", "test123");
}

@Test()
public void TC_5() {
	System.out.println("This the Second TestNG class");
}

@Test()
public void TC_6() {
	System.out.println("This the third TestNG class");
}
	
@AfterMethod
public void closBrowser() {
System.out.println("This is close Browser");
}

@BeforeClass


public void start() {
System.out.println("Ready for test");
}

}
